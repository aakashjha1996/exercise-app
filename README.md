# Exercise Tracker

This is an exercise monitoring and tracking app built with Angular 5 and Anglar Material.

# How to Use ?

- Clone or download the repository.
- Run <code>npm install</code> in the root folder of the application to install dependencies.
- Run <code>ng serve</code> in the root folder to access development server running at localhost:4200
- Navigate to <a href="http://localhost:4200">localhost:4200</a> to access the application.
