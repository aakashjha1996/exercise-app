import { Component, OnInit, OnDestroy } from '@angular/core';
import { TrainingService } from '../../services/training.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit, OnDestroy {
  ongoingTraining = false;
  exerciseSubscription: Subscription;
  selectedTab = 0;

  constructor(private training: TrainingService) { }

  ngOnInit() {
    this.exerciseSubscription = this.training.exerciseChanged.subscribe( exercise => {
      this.ongoingTraining = exercise ? true : false;
    });
  }

  changeTab() {
    this.selectedTab = 0;
  }

  ngOnDestroy() {
    this.exerciseSubscription.unsubscribe();
  }

}
