import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TrainingService } from '../../../services/training.service';
import { Exercise } from '../exercise.model';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit {

  exercises: Exercise[] = [];

  // @Output() trainingStart = new EventEmitter<void>();
  constructor(private training: TrainingService) { }

  ngOnInit() {
    this.exercises = this.training.getAvailableExcercises();
  }

  onStartTraining(formData: NgForm) {
    // this.trainingStart.emit();
    this.training.startExercise(formData.value.exercise);
  }

}
