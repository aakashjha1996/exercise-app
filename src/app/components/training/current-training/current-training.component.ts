import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CancelDialogComponent } from './cancel-dialog.component';
import { TrainingService } from '../../../services/training.service';
import { Exercise } from '../exercise.model';

@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit {
  progress = 0;
  gap = 1;
  timer: any;

  constructor(private dialog: MatDialog, private trainingService: TrainingService) { }

  ngOnInit() {
    this.stopOrResumeTraining();
  }

  stopOrResumeTraining() {
    const step = this.trainingService.getRunningExercise().duration / 100 * 1000;
    this.timer = setInterval(() => {
      this.progress += this.gap;
      if (this.progress >= 100) {
        this.trainingService.completeExercise();
        clearInterval(this.timer);
      }
    }, step);
  }

  stopTraining() {
    clearInterval(this.timer);
    const dialogRef = this.dialog.open(CancelDialogComponent, {
      data: {progress: this.progress}
    });
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.trainingService.cancelExercise(this.progress);
      } else {
        this.stopOrResumeTraining();
      }
    });
  }

}
