import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-cancel-dialog',
    template: `<h2 mat-dialog-title>Are You Sure ?</h2>
                <mat-dialog-content>
                    <p>You already got {{progress}} %</p>
                </mat-dialog-content>
                <mat-dialog-actions>
                    <button mat-raised-button [mat-dialog-close]="true">Yes</button>
                    <button mat-raised-button [mat-dialog-close]="false">No</button>
                </mat-dialog-actions>`,

})
export class CancelDialogComponent {
    progress: any;
    constructor(@Inject(MAT_DIALOG_DATA) public passedData: any, private selfRef: MatDialogRef<CancelDialogComponent>) {
        this.progress = passedData.progress;
        this.selfRef.disableClose = false;
    }
}
