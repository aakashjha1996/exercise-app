import { Injectable } from '@angular/core';
import { Exercise } from '../components/training/exercise.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TrainingService {
    private availableExercise: Exercise[] = [
        { id: 'crunches', name: 'Crunches', duration: 30, calories: 8 },
        { id: 'touch-toes', name: 'Touch Toes', duration: 180, calories: 15 },
        { id: 'side-lunges', name: 'Side Lunges', duration: 120, calories: 18 },
        { id: 'burpees', name: 'Burpees', duration: 60, calories: 8 }
    ];

    private runningExercise: Exercise;
    private exercises: Exercise[] = [];
    exerciseChanged = new Subject<Exercise>();

    getAvailableExcercises(): Exercise[] {
        return this.availableExercise.slice();
    }

    startExercise(selectedId: string) {
        this.runningExercise = this.availableExercise.find( ex => ex.id === selectedId);
        this.exerciseChanged.next({...this.runningExercise});
    }

    cancelExercise(progress: number) {
        this.exercises.push({...this.runningExercise,
            duration: this.runningExercise.duration * ( progress / 100 ),
            calories: this.runningExercise.calories * ( progress / 100 ),
            date: new Date(),
            state: 'completed'});
        console.log(this.exercises);
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    completeExercise() {
        this.exercises.push({...this.runningExercise,
            date: new Date(),
            state: 'completed'});
        console.log(this.exercises);
        this.runningExercise = null;
        this.exerciseChanged.next(null);
    }

    getExercises() {
        console.log(this.exercises);
        return this.exercises.slice();
    }

    getRunningExercise() {
        return {...this.runningExercise};
    }
}
