import { NgModule } from '@angular/core';
import { MatTabsModule, MatCardModule, MatSelectModule, MatProgressSpinnerModule, MatDialogModule, MatTableModule } from '@angular/material';
import { MatNativeDateModule, MatCheckboxModule, MatSidenavModule, MatToolbarModule, MatListModule } from '@angular/material';
import { MatInputModule, MatFormFieldModule, MatButtonModule, MatIconModule, MatDatepickerModule } from '@angular/material';

@NgModule({
    imports: [ MatInputModule,
        MatFormFieldModule,
         MatButtonModule,
          MatIconModule,
           MatDatepickerModule,
            MatNativeDateModule,
             MatCheckboxModule,
             MatSidenavModule,
             MatToolbarModule,
             MatListModule,
             MatTabsModule,
             MatCardModule,
             MatSelectModule,
             MatProgressSpinnerModule,
             MatDialogModule,
             MatTableModule
            ],
    exports: [ MatInputModule,
        MatFormFieldModule,
         MatButtonModule,
          MatIconModule,
           MatDatepickerModule,
            MatNativeDateModule,
             MatCheckboxModule,
             MatSidenavModule,
             MatToolbarModule,
             MatListModule,
             MatTabsModule,
             MatCardModule,
             MatSelectModule,
             MatProgressSpinnerModule,
             MatDialogModule,
             MatTableModule
            ]
})

export class MaterialModule { }
